# Follow the Steps to Provision the AWS Infrastructure for counter app

## Create an S3 Bucket in Singapore region to store the terraform state files and Artifacts.
```shell
aws s3api create-bucket --bucket counter-app-terraform-tfstates --create-bucket-configuration LocationConstraint=ap-southeast-1

```

## Create an Dynamo DB table for State Locking
```shell
aws dynamodb create-table --table-name counter-app --attribute-definitions AttributeName=LockID,AttributeType=S --key-schema AttributeName=LockID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --region ap-southeast-1
```

## Execute Below Terraform commands to init, plan and apply 
```shell
terraform -chdir=./environments/dev init
terraform -chdir=./environments/dev plan
terraform -chdir=./environments/dev apply

```
