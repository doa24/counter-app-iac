# ---- Global vars
variable "aws_region" {
  default = "ap-southeast-1"
}
variable "aws-infra-profile" {}
variable "project" {}
variable "environment" {}

# ---- network variables
variable "doa_vpc_cidr" {}
variable "doa_subnet_web01_public_cidr" {}
variable "doa_subnet_web02_public_cidr" {}
variable "doa_subnet_app01_private_cidr" {}
variable "doa_subnet_app02_private_cidr" {}
variable "doa_subnet_db01_private_cidr" {}
variable "doa_subnet_db02_private_cidr" {}

# --- Variable for EC2
variable "ami" {}
variable "instance_type" {}

