# --- root/main.tf --- 

# Deploy Networking Resources
module "network" {
  source                        = "../../modules/network"
  doa_vpc_cidr                  = var.doa_vpc_cidr
  doa_subnet_web01_public_cidr  = var.doa_subnet_web01_public_cidr
  doa_subnet_web02_public_cidr  = var.doa_subnet_web02_public_cidr
  doa_subnet_app01_private_cidr = var.doa_subnet_app01_private_cidr
  doa_subnet_app02_private_cidr = var.doa_subnet_app02_private_cidr
  doa_subnet_db01_private_cidr  = var.doa_subnet_db01_private_cidr
  doa_subnet_db02_private_cidr  = var.doa_subnet_db02_private_cidr
  project                       = var.project
  environment                   = var.environment
}

# Security Group for ALB
resource "aws_security_group" "alb_security_group" {
  name        = local.alb_security_group_name
  description = "ALB Security Group"
  vpc_id      = module.network.vpc_id

  ingress {
    description = "HTTP from Internet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb_security_group"
  }
}

# Security Group for ASG
resource "aws_security_group" "asg_security_group" {
  name        = local.asg_security_group_name
  description = "ASG Security Group"
  vpc_id      = module.network.vpc_id

  ingress {
    description     = "HTTP from ALB"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_security_group.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "asg_security_group"
  }
}

# Launch Template and ASG Resources

resource "aws_launch_template" "launch_template" {
  name          = local.launch_template_name
  image_id      = var.ami
  instance_type = var.instance_type

  network_interfaces {
    device_index    = 0
    security_groups = [aws_security_group.asg_security_group.id]
    associate_public_ip_address = false
  }
  tag_specifications {
    resource_type = "instance"

    tags = {
    Name = local.launch_template_ec2_name
    }
  }
  iam_instance_profile {
    name = aws_iam_instance_profile.doa_instance_profile.name
  }
  user_data = filebase64("${path.module}/userdata.sh")
}

resource "aws_autoscaling_group" "auto_scaling_group" {
  name                = "doa-auto-scaling-group"
  desired_capacity    = 3
  max_size            = 5
  min_size            = 3
  vpc_zone_identifier = [module.network.doa_subnet_app01_private_id, module.network.doa_subnet_app01_private_id]
  target_group_arns   = [aws_lb_target_group.target_group.arn]

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = aws_launch_template.launch_template.latest_version
  }
}


# Application Load Balancer Resources
resource "aws_lb" "alb" {
  name               = local.alb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_security_group.id]
  subnets            = [module.network.doa_subnet_web01_public_id, module.network.doa_subnet_web02_public_id]
}

resource "aws_lb_target_group" "target_group" {
  name     = local.target_group_name
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.network.vpc_id

  health_check {
    path    = "/"
    matcher = 200
  }
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}
