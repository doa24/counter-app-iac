# --- Global vars value
aws-infra-profile   = "aws-infra-profile"
project = "counter-app"
environment = "dev"

# --- Network vars value
doa_vpc_cidr                  = "10.1.0.0/16"
doa_subnet_web01_public_cidr  = "10.1.1.0/24"
doa_subnet_web02_public_cidr  = "10.1.2.0/24"
doa_subnet_app01_private_cidr = "10.1.3.0/24"
doa_subnet_app02_private_cidr = "10.1.4.0/24"
doa_subnet_db01_private_cidr  = "10.1.5.0/24"
doa_subnet_db02_private_cidr  = "10.1.6.0/24"

ami = "ami-05b46bc4327cf9d99"
instance_type = "t2.micro"

