locals {
  alb_security_group_name  = "alb-security-group"
  asg_security_group_name  = "asg-security-group"
  launch_template_name     = "launch-template"
  launch_template_ec2_name = "asg-ec2"
  alb_name                 = "external-alb"
  target_group_name        = "alb-target-group"
}
