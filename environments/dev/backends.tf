terraform {
  backend "s3" {
    bucket         = "counter-apps-terraform-tfstates"
    key            = "counter-apps-iac/dev/terraform.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "counter-app"
  }
}
