#!/bin/bash
sudo yum update -y
sudo yum install nginx -y
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl status nginx
sudo rm -rf /usr/share/nginx/html/*
sudo aws s3 sync s3://mtanvir-react-app-artifactsv /usr/share/nginx/html/
sudo systemctl restart nginx
